
The collect_nodes modules add both the client and server side of a collecting nodes processus in Drupal 5.

Presentation:
-------------

Collect nodes is about collecting nodes "on the fly", either from a full node (with an additional block) or from a list of nodes (views). It's also about making collections (IE: being able to list, thanks to views integration, the collected nodes).

The huge evolution with this module is that it's based on a FULL (WEB)SERVICE architecture. It's not really going to work without services and json_server properly configured (the first collect_nodes client has been developped in js/jquery/json). The goal here is to have a full gap between the "client" (widget) and the service (database and business logic). Thus, one can make it's own widget (why not flex/air widgets, that'll be easy).

Features:
---------

    * admin configuration: set all the node types which can be collected AND set a maximum amount for a user's collection (and a handler once this limit is over) for normal users to collect nodes (associated with a specific role you can give to your customers). After this limit, you can do what you want (make your customers pay, connect to a workflow, give them points, ...)
    * scalability: a batch_mode has been developped, so that on each view, only one request is done to know in which status the widgets are (collected, not collected yet). Thanks to the webServices architecture, we can imagine a full message queue system at the end (V2?), which will be 100% reliable.
    * usability: the widget has been especially designed for a IE7/Firefox2 use. It's using both directions scalable buttons (borrowed to an internet code, quoted in the css), and it's easy to configure (clean CSS, clean JS,...)
    * views integration: there is already a large views integration (counter, links, filters, ...) and some features are on the way (V2: order by collected_counts like a votingAPI, filter by user-relations-collections
    * workflow-ng integration: events, actions and conditions are declared, so that you can for instance auto-collect a nodes after one guy actually created / modified it.
    * other: and maybe some really cool staff I'm sure I forgot to mention here...

Collect_nodes was designed by Laurent Pipitone (striky2).

Dependencies
------------
[core module]
 * none

[server-side]
 * services

[client-side]
 * json-server

[additional core functionnalities]
* views
* wrkflow-ng

Install
-------

1) Copy the collect_nodes folder to the modules folder in your installation.
2) Enable the modules using Administer -> Modules (/admin/build/modules)
3) Also enable services and json_server (same area)

Configuration
-------------
1) Configure services and json_server (with security key that you need to copy)
2) Sites configuration > collect-nodes > API-key: paste your API key
3) Sites configuration > collect-nodes: default redirection, collection maximum items, nodes that can be collected
4) User / access roles: give proper permissions to your users roles (line collect_nodes)

Contributing & support
------------
Want to contribute new widgets or core evolutions? Want some support?
Post on the issue queue: http://drupal.org/project/issues/collect_nodes