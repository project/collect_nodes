if (Drupal.jsEnabled) {
  $(document).ready(function(){
    
    //On Document ready, get the API KEY
    var api_key = $('.additional-element a.collect_nodes_client_js').eq(0).siblings().filter('span.api-key').attr('id');

  	//On Document ready, call the sessid
  	Drupal.service("system.connect", {api_key: api_key}, function(status, data) {
	  if (status == false) {
	    alert("Fatal error: could not connect to the sytem");
	  }
	  else {
	    parentdataconnection = data;
	    
	  	//batch mode: create an array with all the nid to check of the page
	  	var nid_list = new String();
	  	var i=0;
	  	$('.additional-element a.collect_nodes_client_js').each(function(){
	  	  nid_list += $(this).attr('id') + ",";
	  	  i++;
	  	});
	  	
	  	if (i > 1){	  	
		  nid_list = nid_list.substring(0, nid_list.length-1);
		  	
		  //call batch request service
		  Drupal.service("collect_nodes_service.request_status_batch", {api_key: api_key,  sessid: parentdataconnection.sessid, nid_list: nid_list}, function(status, data) {
		    if (status == false) {
			  Drupal.service("collect_nodes_service.get_error", {api_key: api_key, sessid: parentdataconnection.sessid, error_code: data}, function(status, data) {
	      	  	$('.messages').remove();
	      	  	pager_top.prepend(data['#return'].error_name);
	      	  	img_object.css('display','none');
	      	  	return false;
	      	  }); //end Drupal.service get_error
			}
			else {
			  //make changes with the batch returned array	    
			  temp = Drupal.parseJson(data['#return'].requested_status);		    
			  for (var i=0; i < temp.length; ++i){
			    var a_object = $('.additional-element #' + temp[i].nid);
			    var context_object = a_object.children().filter('span');
			    var img_object = a_object.siblings().filter('img.waiting');
			    if (temp[i].status == true){
	  	          change_status(a_object, context_object, 'uncollect');
			    }
			    img_object.css('display','none');
			  }
			}
		  });
	  	} //end if i > 1
	  	else {
	  	  //only one actually: not batch load widget style
	  	  $('.additional-element a.collect_nodes_client_js').each(function(){	  	    
	  	    var span_object_method = $(this).siblings().filter('span.method');
	  		var img_object = $(this).siblings().filter('img.waiting');
	  		var a_object = $(this);
	  		var context_object = a_object.children().filter('span');
	  		var id = a_object.attr('id');
	  		var method_name = span_object_method.attr('id');

			Drupal.service("collect_nodes_service.request_status", {api_key: api_key,  sessid: parentdataconnection.sessid, nid: id}, function(status, data) {
			  if (status == false) {
				Drupal.service("collect_nodes_service.get_error", {api_key: api_key, sessid: parentdataconnection.sessid, error_code: data}, function(status, data) {
		      	  $('.messages').remove();
		      	  pager_top.prepend(data['#return'].error_name);
		      	  img_object.css('display','none');
		      	  return false;
		      	}); //end Drupal.service get_error
		      }
			  else if (data['#return'].requested_status == 1) {
				//make changes with the batch returned array	    
		        change_status(a_object, context_object, 'uncollect');
			  };
			  img_object.css('display','none');
			})
	  	  });
	  	}//end not batch load widget style
	 }
   }); //end system.connect
  	
  	/*
  	  On collect / unCollect click, use the full JSON Service call
  	*/
  	$('.additional-element .json_service').click(function(){
	  //var id = $(this).parent('div').attr('id');
	  //multi-function handler
	  var span_object_method = $(this).siblings().filter('span.method');
	  var img_object = $(this).siblings().filter('img.waiting');
	  var a_object = $(this);
	  var context_object = a_object.children().filter('span');
	  var id = a_object.attr('id');
	  var method_name = span_object_method.attr('id');
	  var pager_top = $('#tabs-wrapper');
	  
	  span_object_method.each(function(){ //get the span method	    
	    Drupal.service("system.connect", {api_key: api_key}, function(status, data) {
	      if (status == false) {
	      	alert("Fatal error: could not connect to the sytem");
	      }
	      else {
	      	parentdataconnection = data;
		  	Drupal.service(method_name, {api_key: api_key, sessid: parentdataconnection.sessid, nid: id},
		    //call either collect_nodes_service.collect or collect_nodes_service.uncollect
		    function(status, data) {
		      if (status == false) {
		      	//specific error client handling (case: service oversize limit => redirect to param choice)
		      	if (data == 4){
		      	  //call and display error redirection details
		      	  Drupal.service("collect_nodes_service.get_error", {api_key: api_key, sessid: parentdataconnection.sessid, error_code: data},
		    	  function(status, data) {
		      	  	$('.messages').remove();
		      	  	//pager_top.prepend(data);
		      	  	if (data['#return'].redirect_url == '<none>'){
		      	  	  pager_top.prepend(data['#return'].error_name);
		      	  	  img_object.css('display','none');
		      	  	  return false;
		      	  	}
		      	  	else{ 
			      	  //do business redirection
			      	  window.location.replace(data['#return'].redirect_url);
		      	  	}
		      	  }); //end Drupal.service get_error
		      	}
		      	//usual error handling
		      	else{
		      	  //call and display error details
		      	  Drupal.service("collect_nodes_service.get_error", {api_key: api_key, sessid: parentdataconnection.sessid, error_code: data},
		    	  function(status, data) {
		      	  	$('.messages').remove();
		      	  	pager_top.prepend(data['#return'].error_name);
		      	  	img_object.css('display','none');
		      	  	return false;
		      	  }); //end Drupal.service get_error
		      	}
		      }
		      //no error
		      else {
		        img_object.css('display','none');
		        change_status(a_object, context_object, data['#return'].next_code);
		      	$('.messages').remove();
		      	pager_top.prepend(data['#message']);
		      }
		      return false;
		    }); //end Drupal.service collect_nodes_service.collect/uncollect
		  }
		  return false;
	    }); //end Drupal.service system.connect
	    
	  });	  	  
    });
  });

  function change_status(a_object, context_object, op){
  	/* Changes the status of the additional element object to the "op" parameter */
  	
    if (op == 'collect'){
      context_object.html("&nbsp;&nbsp;Collect").prepend("<i></i><span></span>");
	  a_object.removeClass('blue').removeClass('grey').removeClass('green').removeClass('Pink').
	  removeClass('add').removeClass('remove').
	  addClass("blue").addClass("uncollect").addClass("add");
    }
    else if (op == 'uncollect'){
      context_object.html("&nbsp;&nbsp;unCollect").prepend("<i></i><span></span>");
	  a_object.removeClass('blue').removeClass('grey').removeClass('green').removeClass('Pink').
	  removeClass('add').removeClass('remove').
	  addClass("grey").addClass("uncollect").addClass("remove");
    }
  }
}