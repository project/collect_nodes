<?php

/**
 * By default, if a user has the perm 'collect nodes service restricted', he can collect 200 items only.
 */
define('COLLECT_NODES_DEFAULT_RESTRICTION', 200);
/**
 * By default, if an oversized error is detected, the redirection is leading to HOME.
 */
define('COLLECT_NODES_RESTRICTION_OVERSIZED_HANDLER', '<none>');
/**
 * Define error codenames/values
 */
define('BAD_PARAMETER_ERROR', 1);
define('NO_ACCESS_ERROR', 2);
define('NOT_COLLECTABLE_NODE', 3);
define('RESTRICTION_OVERSIZED_ERROR', 4);
define('INCORRECT_OPERATION_NAME_ERROR', 5);
define('INCORRECT_OPERATION_REQUEST_ERROR', 6);

/***************************/
/** Default hooks section **/
/***************************/

/**
 * Implementation of hook_help().
 */
function collect_nodes_help($section) {
  switch ($section) {
    case 'admin/help#collect_nodes':
    case 'admin/modules#description':
      return t('allows user to "put nodes in" / "remove nodes from" their collection.');
  }
}

/**
 * Implementation of hook_menu()
 */
function collect_nodes_menu($may_cache) {
  $items = array();

  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/settings/collect_nodes',
      'title' => t("Collect nodes admin"),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('collect_nodes_admin_settings'),
      'type' => MENU_NORMAL_ITEM,
      'access' => user_access('collect nodes admin'),
    );
    $items[] = array(
      'path' => 'collect-nodes/collect',
      'callback' => 'collect_nodes_collect_page',
      'type' => MENU_CALLBACK,
      'access' => user_access('collect nodes restricted') || user_access('collect nodes unlimited'),
    );
    $items[] = array(
      'path' => 'collect-nodes/uncollect',
      'callback' => 'collect_nodes_uncollect_page',
      'type' => MENU_CALLBACK,
      'access' => user_access('collect nodes restricted') || user_access('collect nodes unlimited'),
    );
  }
  return $items;
}

/**
 * Implementation of hook_perm()
 */
function collect_nodes_perm() {
  return array('collect nodes restricted', 'collect nodes unlimited', 'collect nodes admin', );
}

/**
 * Implementation of hook_admin()
 *
 */
function collect_nodes_admin_settings() {
  $form['collect_nodes_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Configure collect_nodes settings'),
    '#collapsible' => FALSE,
  );
  $form['collect_nodes_settings']['collect_nodes_node_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Collectable node types'),
    '#description' => t('Set the node types which are collectable.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['collect_nodes_settings']['collect_nodes_node_types']['collect_nodes_on'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Available node types'),
    '#options' => node_get_types('names'),
    '#description' => t('Choose the collection/nodes types you want to use with collect nodes'),
    '#default_value' => variable_get('collect_nodes_on', array()),
  );
  $form['collect_nodes_settings']['collect_nodes_default_restriction'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum collected items allowed by person'),
    '#description' => t('Associated with the "collect nodes restricted" perm only. 
    "collect nodes unlimited" perm is not constrained to this rule'),
    '#default_value' => variable_get('collect_nodes_default_restriction', COLLECT_NODES_DEFAULT_RESTRICTION),
  );
  $form['collect_nodes_settings']['collect_nodes_restriction_oversized_handler'] = array(
    '#type' => 'textfield',
    '#title' => t('Handler URL once maximum is reached'),
    '#description' => t('Enter the URL where you want the user to be redirected 
    if the user goes over the restriction (only for "collect nodes restricted perm")<br/>
    If you select "&lt;none&gt;", there will be no redirection, an error message will be displayed "as is" instead.'),
    '#default_value' => variable_get('collect_nodes_restriction_oversized_handler', COLLECT_NODES_RESTRICTION_OVERSIZED_HANDLER),
  );

  return system_settings_form($form);
}

/**
 * Implementation de hook_node_api().
 *
 * Delete all lines in data base about a node if this one is deleted.
 */
function collect_nodes_nodeapi(&$node, $op, $teaser = NULL, $page = NULL) {
  global $user;

  switch ($op) {
    case 'delete' :
      //be responsible
      db_query("DELETE FROM {collect_nodes} WHERE nid = %d", $node->nid);
      break;
  }
}

/************************/
/** Internal functions **/
/************************/

/**
 * _collect_nodes_add
 * Inserts the collect link into the database
 * Don't call this function if not sanitized before
 *
 * @param $nid
 * 	node nid
 */
function _collect_nodes_add($nid) {
  global $user;
  //need to be secured before this call
  //(cf. service collect_nodes_collect_service)

  db_query("INSERT INTO {collect_nodes} (nid, uid, last) VALUES (%d, %d, %d)", $nid, $user->uid, time());
  drupal_set_message(t("The item has been added to your Collection"));

  if (module_exists('workflow_ng')) {
    workflow_ng_invoke_event('collect_nodes_node_collected', node_load($nid), $user);
  }

  return TRUE;
}
/**
 * _collect_nodes_throw
 * Remove the collect link from the database
 * Don't call this function if not sanitized before
 *
 * @param $nid
 * 	node nid
 */
function _collect_nodes_throw($nid) {
  global $user;
  //need to be secured before this call
  //(cf. service collect_nodes_collect_service)
  db_query("DELETE FROM {collect_nodes} WHERE nid = %d AND uid = %d", $nid, $user->uid);
  drupal_set_message(t("The item has been removed from your Collection"));

  if (module_exists('workflow_ng')) {
    workflow_ng_invoke_event('collect_nodes_node_uncollected', node_load($nid), $user);
  }

  return TRUE;
}

/**
 * _collect_nodes_check
 * Look into the database to see if a user already owns the item or not
 *
 * @param $nid
 * 	node nid
 * @param $uid
 * 	[optional], user uid
 */
function _collect_nodes_check($nid, $uid = 0) {
  global $user;

  $uid = $uid ? $uid : $user->uid;

  //batch mode (scalability)
  if (is_array($nid)) {
    $sql = "SELECT nid FROM {collect_nodes} WHERE nid IN (%s) AND uid = %d";
    $query = db_query($sql, implode(', ', $nid), $uid);
    $collected_items = array();

    while ($result = db_fetch_array($query)) {
      $collected_items[] = $result['nid'];
    }

    $not_collected_in_the_page = array_diff($nid, $collected_items);
    $collected_in_the_page = array_intersect($nid, $collected_items);

    $return_array = array();
    foreach ($not_collected_in_the_page as $val) {
      $return_array[] = array('nid' => $val, 'status' => 0);
    }
    foreach ($collected_in_the_page as $val) {
      $return_array[] = array('nid' => $val, 'status' => 1);
    }

    return drupal_to_js($return_array);
  }

  //normal mode
  else if (is_numeric($nid)) {
    $sql = "SELECT * FROM {collect_nodes} WHERE nid = %d AND uid = %d";
    $query = db_query($sql, $nid, $uid);

    return db_result($query) ? 1 : 0;
  }

  return FALSE;
}

/**
 * Check if a user is at the limit for collecting things
 *
 * @return boolean
 * 	TRUE if user is on or over the restricted limit / FALSE
 */
function _collect_nodes_restriction_oversized($uid = NULL, $restriction = NULL) {
  global $user;
  $uid = $uid ? $uid : $user->uid;

  $restriction = isset($restriction) ? $restriction : variable_get('collect_nodes_default_restriction', COLLECT_NODES_DEFAULT_RESTRICTION);

  if (user_access('collect nodes unlimited')) {
    return FALSE;
  }

  $query = db_query("SELECT COUNT(*) as num FROM {collect_nodes} WHERE uid = %d", $user->uid);
  $result = db_fetch_object($query);

  return ($result->num >= $restriction) ? TRUE : FALSE;
}

/**
 * public version of the function
 */
function collect_nodes_restriction_oversized($uid, $restriction) {
  return _collect_nodes_restriction_oversized($uid, $restriction);
}

/**
 * Check if a node is collectable
 *
 * @param $type
 *   the node type being collected
 */
function _collect_nodes_node_type_is_collectable($type) {
  $types = variable_get('collect_nodes_on', array());
  return $types[$type] ? TRUE : FALSE;
}

/**
 * _collect_nodes_secure_operation
 * Check all the elements to secure/sanitize the service.
 *
 * @param $nid
 * 	 node nid
 * @param $op
 * 	 [optional] 'add' or 'throw' to be secured, if empty: autodetection mode
 * @param $uid
 *   [optional] user uid
 * @return
 * 	return['error'] = TRUE if there is a security issue, with a return['error_name'] and return['error_code']
 * 	return['error'] = FALSE otherwise
 */
function _collect_nodes_secure_operation($nid, $op, $uid = 0) {
  global $user;

  $uid = $uid ? $uid : $user->uid;
  $node = node_load($nid);
  $error = array();

  //checking user rights
  if (!$node) {
    $error = array('error' => TRUE, 'error_code' => BAD_PARAMETER_ERROR, 'error_name' => 'You tried to collect a content that doesn\'t exist');
  }
  //checking user rights
  else if (!user_access('collect nodes restricted') && !user_access('collect nodes unlimited')) {
    $error = array('error' => TRUE, 'error_code' => NO_ACCESS_ERROR, 'error_name' => 'You don\'t have the required permissions to have access to this service');
  }
  //check if the node type is ok to be collectable
  else if (!_collect_nodes_node_type_is_collectable($node->type)) {
    $error = array('error' => TRUE, 'error_code' => NOT_COLLECTABLE_NODE, 'error_name' => 'You tried to collect a content whose type is not collectable');
  }
  //checking user rights & business restriction

  else if (!user_access('collect nodes unlimited') && user_access('collect nodes restricted') && _collect_nodes_restriction_oversized()) {
    $error = array('error' => TRUE, 'error_code' => RESTRICTION_OVERSIZED_ERROR, 'error_name' => 'Your account doesn\'t allow you to collect more items. Please upgrade your account');
  }
  //Lorenzo : check the operation stereotype
  else if ($op != 'uncollect' && $op != 'collect' && $op != '') {
    $error = array('error' => TRUE, 'error_code' => INCORRECT_OPERATION_NAME_ERROR, 'error_name' => 'You requested this service with an operation that doesn\'t exist');
  }
  else {
    //mode auto-detect completion
    if ($op == '' and _collect_nodes_check($nid, $uid)) {
      //item already exists
      $op == 'uncollect';
    }
    if ($op == '' and (!_collect_nodes_check($nid, $uid))) {
      //item doesn't exist yet
      $op == 'collect';
    }
    if (_collect_nodes_check($nid, $uid) && $op == 'collect') {
      //check if possible to collect the item in this case
      //if the item is already collect-ed, then it should not be possible
      $error = array('error' => TRUE, 'error_code' => INCORRECT_OPERATION_REQUEST_ERROR, 'error_name' => 'You tried to collect a content you already have');
    }
    else if ((!_collect_nodes_check($nid, $uid)) && $op == 'uncollect') {
      //check if possible to uncollect the item in this case
      //if the item is not collect-ed yet, then it shoud not be possible
      $error = array('error' => TRUE, 'error_code' => INCORRECT_OPERATION_REQUEST_ERROR, 'error_name' => 'You tried to remove a content you don\t have yet');
    }
  }

  if ($error['error']) {
    drupal_set_message(t($error['error_name']), 'error');
    return $error;
  }
  else{
    if (module_exists('workflow_ng')) {
      workflow_ng_invoke_event('collect_nodes_attempt_authorized', node_load($nid), $user);
    }
    return array('error' => FALSE);
  }
}

/********************************/
/** Old Fashioned Drupal Pages **/
/********************************/

/**
 * Render the collect (add) page.
 * @deprecated
 */
function collect_nodes_collect_page($nid, $check = FALSE) {
  global $user;

  //Do a security checking if not done before^M
  if (!$check) {
    $security = _collect_nodes_secure_operation($nid, 'collect', $user->uid);
    if ($security['error'] && $security['error_code'] == 4) {
      drupal_goto(variable_get('collect_nodes_restriction_oversized_handler', COLLECT_NODES_RESTRICTION_OVERSIZED_HANDLER));
    }
    else if ($security['error']) {
      drupal_set_message(t($security['error_name']), 'error');
      drupal_goto('node/'. $nid);
    }
  }

  if (is_numeric($nid)) {
    if ($result = _collect_nodes_add($nid)) {
      drupal_set_message(t('The item is in your collection'));
      drupal_goto('node/'. $nid);
    }

  }
  drupal_not_found();
}
/**
 * Render the uncollect (throw) page.
 * @deprecated
 */
function collect_nodes_uncollect_page($nid, $check = FALSE) {
  global $user;

  //Do a security checking if not done before
  if (!$check) {
    $security = _collect_nodes_secure_operation($nid, 'uncollect', $user->uid);
    if ($security['error'] && $security['error_code'] == 4) {
      drupal_goto(variable_get('collect_nodes_restriction_oversized_handler', COLLECT_NODES_RESTRICTION_OVERSIZED_HANDLER));
    }
    if ($security['error']) {
      drupal_set_message(t($security['error_name']), 'error');
      drupal_goto('node/'. $nid);
      return FALSE;
    }
  }

  if (is_numeric($nid)) {
    if ($result = _collect_nodes_throw($nid)) {
      drupal_set_message(t('The item is not in your collection anymore'), 'warning');
    }
    else {
      drupal_goto('node/'. $nid);
    }
  }
}

/*******************************/
/** Workflow-ng hooks section **/
/*******************************/

/**
 * Implementation of hook_event_info()
 */
function collect_nodes_event_info() {
  return array(
    'collect_nodes_attempt_authorized' => array(
      '#label' => t('User\'s operation attempt authorized'),
      '#arguments' => array(
        'node' => array('#entity' => 'node', '#label' => t('Node being collected')),
        'user' => array('#entity' => 'user', '#label' => t('User, whose collect attempt is on')),
  ),
      '#redirect' => TRUE,
      '#module' => t('Collect nodes'),
  ),
    'collect_nodes_node_collected' => array(
      '#label' => t('Node collected'),
      '#arguments' => array(
        'node' => array('#entity' => 'node', '#label' => t('Node collected')),
        'user' => array('#entity' => 'user', '#label' => t('User')),
  ),
      '#redirect' => TRUE,
      '#module' => t('Collect nodes'),
  ),
    'collect_nodes_node_uncollected' => array(
      '#label' => t('Node uncollected'),
      '#arguments' => array(
        'node' => array('#entity' => 'node', '#label' => t('Node uncollected')),
        'user' => array('#entity' => 'user', '#label' => t('User')),
  ),
      '#redirect' => TRUE,
      '#module' => t('Collect nodes'),
  ),
  );
}
/**
 * Implementation of hook_action_info()
 */
function collect_nodes_action_info() {
  return array(
    'collect_nodes_action_node_collect' => array(
      '#label' => t('Collect a content'),
      '#arguments' => array(
        'node' => array('#entity' => 'node', '#label' => t('Content')),
        'user' => array('#entity' => 'user', '#label' => t('User who is collecting the content')),
  ),
      '#module' => t('Collect nodes'),
  ),
    'collect_nodes_action_node_uncollect' => array(
      '#label' => t('Uncollect a content'),
      '#arguments' => array(
        'node' => array('#entity' => 'node', '#label' => t('Content')),
        'collector' => array('#entity' => 'user', '#label' => t('User who is uncollecting the content')),
  ),
      '#module' => t('Collect nodes'),
  ),
  );
}
/**
 * Implementation of hook_condition_info()
 */
function collect_nodes_condition_info() {
  return array(
    'collect_nodes_condition_user_has_access' => array(
      '#label' => t('Evaluate if the user can access the collect_nodes service'),
      '#arguments' => array(
        'user' => array('#entity' => 'user', '#label' => t('User to check')),
  ),
      '#description' => t('Evaluates to TRUE, if the user can access the service.'),
      '#module' => t('Collect nodes'),
  ),
    'collect_nodes_condition_node_collectable' => array(
      '#label' => t('Evaluate if the node type is collectable'),
      '#arguments' => array(
        'node' => array('#entity' => 'node', '#label' => t('Node to check')),
  ),
      '#description' => t('Evaluates to TRUE, if the node type is collectable.'),
      '#module' => t('Collect nodes'),
  ),
    'collect_nodes_condition_collect_authorized_operation' => array(
      '#label' => t('Evaluate if the user can launch the collect operation on this node'),
      '#arguments' => array(
        'node' => array('#entity' => 'node', '#label' => t('Node to check')),
        'user' => array('#entity' => 'user', '#label' => t('User to check')),
  ),
      '#description' => t('Evaluates to TRUE, if the user can run the collect operation.'),
      '#module' => t('Collect nodes'),
  ),
    'collect_nodes_condition_uncollect_authorized_operation' => array(
      '#label' => t('Evaluate if the user can launch the uncollect operation on this node'),
      '#arguments' => array(
        'node' => array('#entity' => 'node', '#label' => t('Node to check')),
        'user' => array('#entity' => 'user', '#label' => t('User to check')),
  ),
      '#description' => t('Evaluates to TRUE, if the user can run the uncollect operation.'),
      '#module' => t('Collect nodes'),
  ),
    'collect_nodes_condition_restriction_oversized' => array(
      '#label' => t('Evaluate if user\'s collection doesn\'t exceed authorized size'),
      '#arguments' => array(
        'user' => array('#entity' => 'user', '#label' => t('User to check')),
  ),
      '#description' => t('Evaluates to TRUE, if the user\'s collection doesn\'t exceed authorised size.'),
      '#module' => t('Collect nodes'),
  ),
  );
}

/**
 * Action to collect a node
 */
function collect_nodes_action_node_collect($node, $user) {
  $nid = $node->nid;

  $security = _collect_nodes_secure_operation($nid, 'collect', $user->uid);
  if ($security['error'] && $security['error_code'] == 4) {
    drupal_goto(variable_get('collect_nodes_restriction_oversized_handler', COLLECT_NODES_RESTRICTION_OVERSIZED_HANDLER));
  }
  else if ($security['error']) {
    drupal_set_message(t($security['error_name']), 'error');
    drupal_goto('node/'. $nid);
  }

  if (is_numeric($nid)) {
    if ($result = _collect_nodes_add($nid)) {
      drupal_set_message(t('The item is in your collection'));
      drupal_goto('node/'. $nid);
    }
  }
  drupal_not_found();
  return array();
}

/**
 * Action to uncollect a node
 */
function collect_nodes_action_node_uncollect($node, $user) {
  $nid = $node->nid;

  $security = _collect_nodes_secure_operation($nid, 'uncollect', $user->uid);
  if ($security['error'] && $security['error_code'] == 4) {
    drupal_goto(variable_get('collect_nodes_restriction_oversized_handler', COLLECT_NODES_RESTRICTION_OVERSIZED_HANDLER));
  }
  if ($security['error']) {
    drupal_set_message(t($security['error_name']), 'error');
    drupal_goto('node/'. $nid);
    return FALSE;
  }

  if (is_numeric($nid)) {
    if ($result = _collect_nodes_throw($nid)) {
      drupal_set_message(t('The item is not in your collection anymore'), 'warning');
    }
    else {
      drupal_goto('node/'. $nid);
    }
  }
}

/**
 * Condition to show if the user can have access to the service
 */
function collect_nodes_condition_user_has_access($user) {
  //check if the user can have access to the service
  if (user_access('collect nodes restricted') || user_access('collect nodes unlimited')) {
    return TRUE;
  }
  else {
    drupal_set_message(t('You don\'t have the required authorizations to have access to this service'));
    return FALSE;
  }
}

/**
 * Condition to show if the node_type is collectable
 */
function collect_nodes_condition_node_collectable($node) {
  //check if the node type is ok to be collectable
  return _collect_nodes_node_type_is_collectable($node->type) ? TRUE : FALSE;
}

/**
 * Condition to show if user can collect a node (ie: node status uncollect)
 */
function collect_nodes_condition_collect_authorized_operation($node, $user) {
  if (_collect_nodes_check($nid, $uid) && $op == 'collect') {
    drupal_set_message(t('You have already collected this content, you can\'t do it again'));
    return FALSE;
  }
  else {
    return TRUE;
  }
}

/**
 * Condition to show if user can uncollect a node (ie: node status collect)
 */
function collect_nodes_condition_uncollect_authorized_operation($node, $user) {
  if (!_collect_nodes_check($nid, $uid) && $op == 'uncollect') {
    drupal_set_message(t('This content is not in you collection, you can\'t remove it'));
    return FALSE;
  }
  else {
    return TRUE;
  }
}

/**
 * Condition to show "collection limit oversized"
 */
function collect_nodes_condition_restriction_oversized($user) {
  if (!user_access('collect nodes unlimited') && user_access('collect nodes restricted') && _collect_nodes_restriction_oversized()) {
    drupal_set_message(t('Your account type doesn\'t allow you to collect more content for now'));
    return FALSE;
  }
  return TRUE;
}

/*************************/
/** Views hooks section **/
/*************************/

/**
 * Implementation of hook_views_tables().
 */
function collect_nodes_views_tables() {
  $tables = array();

  $tables['collect_nodes'] = array(
    'name'            => 'collect_nodes',
    'join'            => array(
      'left'            => array(
        'table'           => 'node',
        'field'           => 'nid'
        ),
      'right'           => array( 
        'field'           => 'nid'
        ),
        ),
    'fields'        => array(
      'last' => array(
        'name' => t('Collect nodes: Time Added'),
        'sortable' => true,
        'handler' => views_handler_field_dates(),
        'option' => 'string',
        'help' => t('Display the date/time the node was added.')
        ),
      'count' => array(
        'name' => t('Collect nodes: Count'),
        'handler' => 'collect_nodes_handler_user_count',
        'help' => t('Number of times this node was collected by any user.'),
        'sortable' => FALSE,
        'notafield' => TRUE,
        ),
      'collect_link' => array(
        'name' => t('Collect nodes: Link'),
        'sortable' => false,
        'help' => t('display the collect nodes link'),
        'handler' => 'collect_nodes_views_handler_field_lnk',
        'notafield' => true,
        ),
        ),
    'sorts'         => array(
      'last'          => array(
        'name'          => t('Collect nodes: last collected'),
        'help'          => t('This allows you to sort chronologically your collection.'),
        )
        ),
    'filters'       => array(
      'last' => array(
        'name'     => t('Collect nodes: Time Added'),
        'operator' => 'views_handler_operator_gtlt',
        'value'    => views_handler_filter_date_value_form(),
        'handler'  => 'views_handler_filter_timestamp',
        'option'   => 'string',
        'help'     => t('This filter allows collected nodes to be filtered by the date and time the user added them. Enter dates in the format: CCYY-MM-DD HH:MM:SS. Enter \'now\' to use the current time. You may enter a delta (in seconds) to the option that will be added to the time; this is most useful when combined with now.'),
        ),
        ),
        );

        return $tables;
}

/**
 * Implementation of hook_handler_user_count().
 */
function collect_nodes_handler_user_count($fieldinfo, $fielddata, $value, $data) {
  return db_result(db_query("SELECT COUNT(*) FROM {collect_nodes} WHERE nid = %d", $data->nid));
}

/**
 * Implementation of view's hook_views_arguments().
 */
function collect_nodes_views_arguments() {
  $arguments['collect_nodes_user'] = array(
    'name' => t('collect nodes: select user'),
    'help' => t('collect nodes: select only the user\'s collected elements'),
    'handler' => 'collect_nodes_views_handler_argument'
    );
    return $arguments;
}

/**
 * Implementation of view's hook_views_handler_argument().
 */
function collect_nodes_views_handler_argument($op, &$query, $a1, $a2=null) {
  if ($a2 == null) {
    global $user;
    $a2 = $user->uid;
  }
  switch ($op) {
    case 'filter' :
      if (is_numeric($a2)) {
        $query->ensure_table('collect_nodes');
        $query->add_where("collect_nodes.uid = $a2");
      }
      break;
  }
}

/**
 * Implementation of view's hook_views_handler_field_lnk().
 */
function collect_nodes_views_handler_field_lnk($fieldinfo, $fielddata, $value, $data) {
  return theme('collect_nodes_client_js', $data);
}