<?php

/**
 * collect_nodes_ur.inc
 *
 * is an extension of the collect nodes module 
 * to ensure a full compatibility with the User Relationships module
 * 
 * @file collect_nodes_ur.inc
 *
 */

/*************************
 ** Views hooks section **
 ************************/

/*************************
 ** EARLY STAGE DEV !!! **
 ************************/

/**
 * Implementation of view's hook_views_tables().
 */
function collect_nodes_ur_views_tables() {
  $tables = array();

  $tables['collect_nodes_ur'] = array(
    'name'            => 'collect_nodes',
    'join'            => array(
      'left'            => array(
        'table'           => 'node',
        'field'           => 'nid'
      ),
      'right'           => array( 
        'field'           => 'nid'
      ),
    ),
    'fields'        => array(
      'uid' => array(
        'name' => t('Collect nodes: user'),
        'sortable' => false,
        'help' => t('Display the userid who collected the node.')
      ),
    ),
    'filters'       => array(
      'user_relationship' => array(  
        'name'     => t('Collect nodes: user_relationships'),
        'handler'  => 'collect_nodes_ur_views_handler_filter',
        'operator' => 'collect_nodes_ur_views_handler_operator_eq',
        'value'     => array(
          '#type'     => 'select',
          '#options'  => 'user_relationship_views_relationship_types'
        ),
        'help'     => t('This filter allows collected nodes to be filtered by relations.'),
      ),
    ),
  );

  return $tables;
}

function collect_nodes_ur_views_handler_operator_eq() {
  return array(
    '='   => t('Is'),
  );
} 

function collect_nodes_ur_views_handler_filter($op, $filter_data, $filter_info, &$query) {
  global $user;
  
  views_handler_filter_default($op, $filter_data, $filter_info, $query);  
  $relations = user_relationships_load(array("user" => $user->uid), FALSE, 'rid', NULL, NULL, FALSE);
  
  $query->ensure_table('collect_nodes_ur');
  $query->add_where('uid IN (%s)', implode(', ', $relations));
}

/**
 * Implementation of view's hook_views_arguments().
 */
function collect_nodes_ur_views_arguments() {
  $arguments['collect_nodes_user_relationships'] = array(
    'name' => t('Collect nodes: take user_relationships nodes'),
    'help' => t('Collect nodes:: select only the user\'s relationshipds collected elements'),
    'handler' => 'collect_nodes_ur_views_handler_argument'
  );
  return $arguments;
}

/*
function collect_nodes_ur_views_query_alter(&$query, &$view, $summary, $level) {
  dump($query);
}
*/

/**
 * Implementation of view's hook_views_handler_argument().
 */
function collect_nodes_ur_views_handler_argument($op, &$query, $a1, $a2=null) {
  if ($a2 == null) {
    global $user;
    $a2 = $user->uid;
  }
  $uids_temp = user_relationships_load(array("user" => $a2));
  $uids = array();
  foreach ($uids_temp as $key => $val) {
    if ($val->requester_id != $a2 && !in_array($val->requester_id, $uids)) {
      $uids[] =  $val->requester_id;
    }
    else if ($val->requestee_id != $a2 && !in_array($val->requestee_id, $uids)) {
      $uids[] = $val->requestee_id;
    }
  }
  
  switch ($op) {
    case 'filter' :
      if (is_numeric($a2)) {
        $query->ensure_table('collect_nodes');
        $query->add_where("collect_nodes.uid IN (". implode(',', $uids) .")");
      }
      break;
  }
}
