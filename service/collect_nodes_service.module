<?php

/**
 * collect_nodes_service.module provides methods & services to add or remove items from one's own node collections
 *
 * collect_nodes_service.module is "auto-pluggable" on each collection type.
 * The main methods are:
 *  - collect_nodes_service_collect_method($nid, $check = FALSE)
 *  - collect_nodes_service_uncollect_method($nid, $check = FALSE)
 *  - collect_nodes_service_collect_uncollect_method($nid, $op)
 *  - collect_nodes_service_request_status_method($nid)
 *  - collect_nodes_service_get_error_method($error_code)
 *
 * @file collect_nodes_service.module
 *
 */

/***************************
 ** Default hooks section **
 **************************/

/**
 * Implementation of hook_help().
 */
function collect_nodes_service_help($section) {
  switch ($section) {
    case 'admin/help#collect_nodes_service':
    case 'admin/modules#description':
      return t('service to allows user to "put nodes in" / "remove nodes from" their collection.');
  }
}

/**********************
 ** Service function **
 *********************/

/**
 * Implementation of hook_service()
 */
function collect_nodes_service_service() {
  return array(
    //collect_nodes_service.request_status service
    // already knows the user (session user only)
    array(
      '#method'   => 'collect_nodes_service.request_status',
      '#callback' => 'collect_nodes_service_request_status_method',
      '#args'     => array(
        array(
          '#name'         => 'nid',
          '#type'         => 'int',
          '#description'  => t('nid of the node to be checked')
        ),
      ),
      '#return'   => 'struct',
      '#help'     => t('Request the status of a node: already collected or not'),
    ),
    //collect_nodes_service.request_status_batch service
    // already knows the user (session user only)
    array(
      '#method'   => 'collect_nodes_service.request_status_batch',
      '#callback' => 'collect_nodes_service_request_status_batch_method',
      '#args'     => array(
        array(
          '#name'         => 'nid_list',
          '#type'         => 'string',
          '#description'  => t('nid array of the nodes to be checked')
        ),
      ),
      '#return'   => 'drupal_to_js_array',
      '#help'     => t('Request the status of some nodes (batch): already collected or not'),
    ),
    // collect_nodes_service.collect_uncollect service
    // already knows the user (session user only)
    array(
      '#method'   => 'collect_nodes_service.collect_uncollect',
      '#callback' => 'collect_nodes_service_collect_uncollect_method',
      '#args'     => array(
        array(
          '#name'         => 'nid',
          '#type'         => 'int',
          '#description'  => t('nid of the node to be collected or uncollected.')
        ),
        array(
          '#name'         => 'op',
          '#type'         => 'string',
          '#optional'     => TRUE, 
          '#description'  => t('operation \'collect\'/\'uncollect\' element')
        )
      ),
      '#return'   => 'struct',
      '#help'     => t('Allow a user to collect or uncollect an element specified by its nid'),
    ),
    array(
      '#method'   => 'collect_nodes_service.get_error',
      '#callback' => 'collect_nodes_service_get_error_method',
      '#args'     => array(
        array(
          '#name'         => 'error_code',
          '#type'         => 'int',
          '#description'  => t('The code error thas has been previously returned')
        ),
      ),
      '#return'   => 'struct',
      '#help'     => t('Allow a programmer to get the error string from its error_code id. 
      (only if the error has not been printed yet by Drupal)'),
    ),
  );
}

/**
 * collect_nodes_service_collect_method
 * Method for item collect (add element method).
 *
 * @author Lorenzo
 * @date 2007-11-23
 * @param $nid
 * 	nid of the node
 * @param $check
 *  if TRUE, the security check (user rights, collection rights, ...) has been done already.
 */
function collect_nodes_service_collect_method($nid, $check = FALSE) {
  global $user;

  //do a security check if not done before
  if (!$check) {
    $security = _collect_nodes_secure_operation($nid, 'collect', $user->uid);
    if ($security['error']) {
      $response['#message'] = $security['error_code'];
      $response['#error'] = TRUE;
    }
  }
  
  if (!$response['#error']) {
    $done = _collect_nodes_add($nid);
    
    $response = array(
      '#message' => theme_status_messages(),
      '#return' => array(
        'next_code' => 'uncollect',
      ),
    );
  }
  
  //operation status and modification report
  return $response;
}

/**
 * collect_nodes_service_uncollect_method
 * Method for item uncollect (throw element method).
 *
 * 
 * @date 2007-11-23
 * @param $nid
 * 	nid of the node
 * @param $check
 *  if TRUE, the security check (user rights, collection rights, ...) has been done already.
 */
function collect_nodes_service_uncollect_method($nid, $check = FALSE) {
  global $user;

  //do a security check if not done before
  if (!$check) {
    $security = _collect_nodes_secure_operation($nid, 'uncollect', $user->uid);
    if ($security['error']) {
      $response['#message'] = $security['error_code'];
      $response['#error'] = TRUE;
    }
  }
  
  if (!$response['#error']) {
    $done = _collect_nodes_throw($nid);
    
    $response = array(
      '#message' => theme_status_messages('status'),
      '#return' => array(
        'next_code' => 'collect',
      ),
    );
  }
  
  //operation status and modification report
  return $response;
}

/**
 * collect_nodes_service_collect_uncollect_method
 * Method for item collect or uncollect (op auto detection possible).
 *
 * 
 * @date 2007-11-23
 * @param $nid
 * 	nid of the node
 * @param $op
 *  'collect'/'uncollect', if empty, auto detection.
 */
function collect_nodes_service_collect_uncollect_method($nid, $op = '') {
  global $user;
  
  //mode auto-select if bad parameter name
  if ($op != 'collect' && $op != 'uncollect') {
    $op = '';
  }
  
  //do a security checking if not done before, auto-detect mode included
  $security = _collect_nodes_secure_operation($nid, $op, $user->uid);
  if ($security['error']) {
    $response['#error'] = TRUE;
    $response['#message'] = $security['error_code'];
  }
  else {
    switch ($op) {
      case '' :
        //mode action auto-detect
        if (_collect_nodes_check($nid, $user->uid)) {
          //item already collect-ed
          $response = collect_nodes_service_uncollect_method($nid, TRUE); 
          //verification done
        }
        else {
          $response = collect_nodes_service_collect_method($nid, TRUE);
          if (module_exists('workflow_ng')) {
            workflow_ng_invoke_event('collect_nodes_service_node_collected', node_load($nid), $user);
          } 
          //verification done
        }
        break;
  
      case 'collect':
        //mode call collect method
        if (module_exists('workflow_ng')) {
          workflow_ng_invoke_event('collect_nodes_service_collect_attempt_authorized', node_load($nid), $user);
        }
        $response = collect_nodes_service_collect_method($nid, TRUE);
        if (module_exists('workflow_ng')) {
          workflow_ng_invoke_event('collect_nodes_service_node_collected', node_load($nid), $user);
        }
        break;
  
      case 'uncollect':
      	//mode call uncollect method
        $response = collect_nodes_service_uncollect_method($nid, TRUE);
        if (module_exists('workflow_ng')) {
          workflow_ng_invoke_event('collect_nodes_service_node_uncollected', node_load($nid), $user);
        }
        break;
  
      default:
        break;
    }
  }
  
  //operation status and next operation report
  return $response;
}

/**
 * collect_nodes_service_get_error_method
 *
 * 
 * @date 2007-11-23
 * @param $error_code
 * 	the code error wich string is looked for.
 */
function collect_nodes_service_get_error_method($error_code) {
  global $base_url;
  
  $error_name = theme_status_messages('error');
  
  //specific error type: add some handler
  if ($error_code == 4) {
    $redirect_url = variable_get('collect_nodes_restriction_oversized_handler', COLLECT_NODES_RESTRICTION_OVERSIZED_HANDLER);
    return array(
      '#message' => $error_name,
      '#return' => array(
        'redirect_url' => $redirect_url == '<none>' ? '<none>' : $base_url .'/'. $redirect_url,
        'error_name' => $error_name,
        'error_code' => $error_code,
      ),
    );
  }
  //usual error type, only send the message
  //else {
    return array(
      '#message' => $error_name,
      '#return' => array(
        'error_name' => $error_name,
        'error_code' => $error_code,
      ),
    );
  //}
}
/**
 * collect_nodes_service_request_status_method
 * check if the node (item) has already been collected by the user or not
 * 
 * @param $nid
 */
function collect_nodes_service_request_status_method($nid) {
  return array(
    '#return' => array(
      'requested_status' => _collect_nodes_check($nid),
    ),
  );
}
/**
 * collect_nodes_service_request_status_batch_method
 * check if some nodes (array of nodes) have already been collected by the user or not
 * 
 * @param $nid (array)
 */
function collect_nodes_service_request_status_batch_method($nid_list) {
  //make an array from the list
  $nid_array = explode('%2C', $nid_list);
  
  return array(
    '#return' => array(
      'requested_status' => _collect_nodes_check($nid_array),
    ),
  );
}
